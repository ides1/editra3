class BaseMeta(type):
    def __new__(cls, classname, bases, dict):
        new = type.__new__(cls, classname, bases, dict)
        new.classname = classname
        print("BaseMeta::new. Called.")
        return new


class Base(object, metaclass=BaseMeta):
    __metaclass__ = BaseMeta


class HeirMeta(BaseMeta, Base):
    def __new__(self, *args):
        new = BaseMeta.__new__(self, *args)
        print("HeirMeta::new. Called.")
        return new

    def test(self):
        print("test")


# class Heir(Base):
#     __metaclass__ = HeirMeta

#     @classmethod
#     def define(cls, nexttype):
#         print("Heir::define. Called.")


# class HeirOfHeir(Heir):
#     pass


# Heir.define(HeirOfHeir)
hm = HeirMeta()
hm.test()
# HeirMeta.test(HeirMeta)
